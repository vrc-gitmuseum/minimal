museumの最小構成を置くリポジトリです。マージの実験などに使用しますが、そのために必要なものだけをmasterにマージし、それ以外はブランチでの実験をしてください

# 実行に必要なアセット

* VRC SDK https://www.vrchat.net/home/download
* Free HDR Sky https://assetstore.unity.com/packages/2d/textures-materials/sky/free-hdr-sky-61217
* ProBuilder https://assetstore.unity.com/packages/tools/modeling/probuilder-111418
